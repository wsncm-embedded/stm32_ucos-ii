 /**
 * @brief       : 定时器
 *                现象: 每隔一秒打印一次数据
 *
 * @file        : app_case_timer.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/10/12  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

OS_STK app_task_start_stk[APP_TASK_START_STK_SIZE];
OS_STK app_timer_task_stk[App_TASK_COMMON_STK_SIZE];

static void app_task_create(void);
static void app_event_create(void);
static void app_task_start(void *p_arg);
static void app_timer_create(void);

OS_EVENT *sem_isr;
OS_TMR *pTimer0;

int main(void)
{
    BSP_IntDisAll();                                            
    OSInit();                                                  
    OSTaskCreate(
                (void (*)(void *)) app_task_start, 
                (void * ) 0,
                (OS_STK* )&app_task_start_stk[APP_TASK_START_STK_SIZE - 1],
                (INT8U) APP_TASK_START_PRIO
                );
                
    OSStart();
    return (0);
}

/**
 * @brief 启动任务，单次执行
 */
void app_task_start(void *p_arg)
{
    (void)p_arg;
    OS_CPU_SysTickInit();  
    BSP_Init();
    app_event_create();
    app_task_create();
    app_timer_create();
    while (1)                                            
    {
        printf("start task!\r\n");
        OSTimeDly(500);
        OSTaskDel(OS_PRIO_SELF);
    }
}

/**
 * @brief 定时器回调函数
 */
void app_timer_cb(void *ptmr, void *parg)
{
    static uint32_t run_count =  0;
    printf("Timer :%d\r\n",run_count + 1);
    RUN_COUNTER(run_count);
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    sem_isr = OSSemCreate(0);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{
                
}

/**
 * @brief 创建定时器
 */
static void app_timer_create(void)
{
    INT8U err = 0; 
    
    pTimer0 = OSTmrCreate
    (
        0,
        10,
        OS_TMR_OPT_PERIODIC,
        (OS_TMR_CALLBACK)app_timer_cb,
        (INT8U *)0,
        "OSTIMER0",
        &err
    );
    
    if(err == OS_ERR_NONE)
    {
        printf("creat timer succsee!\r\n");
    }

    OSTmrStart(pTimer0, &err);
    if(err == OS_ERR_NONE)
    {
        printf("start timer succsee!\r\n");
    }
}

#if (OS_APP_HOOKS_EN > 0)

void App_TaskCreateHook(OS_TCB *ptcb)
{

}

void App_TaskDelHook(OS_TCB *ptcb)
{
    (void)ptcb;
}

#if OS_VERSION >= 251
void App_TaskIdleHook(void)
{
}
#endif

void App_TaskStatHook(void)
{
}


#if OS_TASK_SW_HOOK_EN > 0
void App_TaskSwHook(void)
{

}
#endif

#if OS_VERSION >= 204
void App_TCBInitHook(OS_TCB *ptcb)
{
    (void)ptcb;
}
#endif


#if OS_TIME_TICK_HOOK_EN > 0
void App_TimeTickHook(void)
{

}
#endif
#endif
