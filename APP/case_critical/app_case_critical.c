 /**
 * @brief       : 临界区保护
 *                开关中断：
 *                调度器上锁：测试任务周期执行，顺序打印ABC则测试通过
 *                互斥信号量：测试任务周期执行，顺序打印ABC则测试通过
 * @file        : app_case_critical.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/10/12  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

OS_STK app_task_start_stk[APP_TASK_START_STK_SIZE];
OS_STK app_close_isr_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_lock_sched_test_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_lock_sched_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_mutex_sem_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_mutex_test_task_stk[App_TASK_COMMON_STK_SIZE];

static void app_task_create(void);
static void app_event_create(void);
static void app_task_start(void *p_arg);

OS_EVENT *sem_isr;
OS_EVENT *sem_lock_sched;
OS_EVENT *sem_task;
OS_EVENT *sem_mutex;

int main(void)
{
    BSP_IntDisAll();                                            
    OSInit();                                                  
    OSTaskCreate(
                (void (*)(void *)) app_task_start, 
                (void * ) 0,
                (OS_STK* )&app_task_start_stk[APP_TASK_START_STK_SIZE - 1],
                (INT8U) APP_TASK_START_PRIO
                );
                
    OSStart();
    return (0);
}

/**
 * @brief 启动任务，单次执行
 */
void app_task_start(void *p_arg)
{
    (void)p_arg;
    OS_CPU_SysTickInit();  
    BSP_Init();
    app_event_create();
    app_task_create();
    while (1)                                            
    {
        printf("start task!\r\n");
        OSTimeDly(500);
        OSTaskDel(OS_PRIO_SELF);
    }
}

/**
 * @brief 关中断方式保护资源
 */
void app_close_isr_task(void *p_arg)
{
    while (1)                                            
    {
        OS_CPU_SR  cpu_sr;
        OS_ENTER_CRITICAL();// 关中断
        printf("ISR A\r\n");
        OS_EXIT_CRITICAL();// 开中断
        OSTimeDly(DELAY_TIMER);

        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 调度器上锁方式测试
 */
void app_lock_sched_test_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSSemPend(sem_lock_sched, 0, &err);
        printf("lock sched C\r\n");
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 调度器上锁方式保护资源
 */
void app_lock_sched_task(void *p_arg)
{
    while (1)                                            
    {
        OSSchedLock();
        printf("lock sched A\r\n");
        OSSemPost(sem_lock_sched);
        printf("lock sched B\r\n");
        OSSchedUnlock();

        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OSTimeDly(DELAY_TIMER);
    }
}

/**
 * @brief 互斥信号量方式测试（优先级高于被测试任务）
 */
void app_mutex_test_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSSemPend(sem_task, 0, &err);
        OSMutexPend(sem_mutex, 0, &err);
        printf("mutex C\r\n");
        OSMutexPost(sem_mutex);
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 互斥信号量方式保护资源
 */
void app_mutex_sem_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSMutexPend(sem_mutex, 0, &err);
        printf("mutex A\r\n");
        OSSemPost(sem_task);
        printf("mutex B\r\n");
        OSMutexPost(sem_mutex);
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OSTimeDly(DELAY_TIMER);
    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    INT8U err;
    sem_isr = OSSemCreate(0);
    sem_task = OSSemCreate(0);
    sem_lock_sched = OSSemCreate(0);
    sem_mutex = OSMutexCreate(2, &err);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{ 
    OSTaskCreate(
                (void (*)(void *)) app_close_isr_task, 
                (void * ) 0,
                (OS_STK* )&app_close_isr_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO
                );
    
    OSTaskCreate(
                (void (*)(void *)) app_lock_sched_test_task, 
                (void * ) 0,
                (OS_STK* )&app_lock_sched_test_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 1
                );

    OSTaskCreate(
                (void (*)(void *)) app_mutex_test_task, 
                (void * ) 0,
                (OS_STK* )&app_mutex_test_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 2
                );
                
    OSTaskCreate(
                (void (*)(void *)) app_lock_sched_task, 
                (void * ) 0,
                (OS_STK* )&app_lock_sched_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 3
                );
                
    OSTaskCreate(
                (void (*)(void *)) app_mutex_sem_task, 
                (void * ) 0,
                (OS_STK* )&app_mutex_sem_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 4
                );
}



#if (OS_APP_HOOKS_EN > 0)

void App_TaskCreateHook(OS_TCB *ptcb)
{

}

void App_TaskDelHook(OS_TCB *ptcb)
{
    (void)ptcb;
}

#if OS_VERSION >= 251
void App_TaskIdleHook(void)
{
}
#endif

void App_TaskStatHook(void)
{
}


#if OS_TASK_SW_HOOK_EN > 0
void App_TaskSwHook(void)
{

}
#endif

#if OS_VERSION >= 204
void App_TCBInitHook(OS_TCB *ptcb)
{
    (void)ptcb;
}
#endif


#if OS_TIME_TICK_HOOK_EN > 0
void App_TimeTickHook(void)
{

}
#endif
#endif
