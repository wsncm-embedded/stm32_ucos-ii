 /**
 * @brief       : 行为同步
 *                中断任务同步: 串口中发送'1'，会同步任务进行打印
 *                任务单向同步: 单向同步依次打印ABC
 *                任务双向同步: 按优先级依次打印
 *                任务事件同步: 按顺序打印ABC
 * @file        : app_case_sync.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/10/12  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

OS_STK app_task_start_stk[APP_TASK_START_STK_SIZE];

OS_STK app_isr_sync_task_stk[App_TASK_COMMON_STK_SIZE];

OS_STK app_sync_a_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_sync_b_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_sync_c_task_stk[App_TASK_COMMON_STK_SIZE];

OS_STK app_bisynchronous_a_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_bisynchronous_b_task_stk[App_TASK_COMMON_STK_SIZE];

OS_STK app_flag_sync_a_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_flag_sync_b_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_flag_sync_c_task_stk[App_TASK_COMMON_STK_SIZE];


static void app_task_create(void);
static void app_event_create(void);
static void app_task_start(void *p_arg);

OS_EVENT *sem_isr;
OS_EVENT *sem_sync_a;
OS_EVENT *sem_sync_b;
OS_EVENT *sem_sync_c;

OS_EVENT *sem_box;
OS_EVENT *ack_box;

OS_FLAG_GRP *flagw;

int main(void)
{
    BSP_IntDisAll();                                            
    OSInit();                                                  
    OSTaskCreate(
                (void (*)(void *)) app_task_start, 
                (void * ) 0,
                (OS_STK* )&app_task_start_stk[APP_TASK_START_STK_SIZE - 1],
                (INT8U) APP_TASK_START_PRIO
                );

    OSStart();
    return (0);
}

/**
 * @brief 启动任务，单次执行
 */
void app_task_start(void *p_arg)
{
    (void)p_arg;
    OS_CPU_SysTickInit();  
    BSP_Init();
    app_event_create();
    app_task_create();
    while (1)                                            
    {
        printf("start task!\r\n");
        OSTimeDly(DELAY_TIMER);
        OSTaskDel(OS_PRIO_SELF);
    }
}

/**
 * @brief ISR同步任务
 */
void app_isr_sync_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSSemPend(sem_isr, 0, &err);
        OSSchedLock();
        printf("isr sync task\r\n");
        OSSchedUnlock();
    }
}

/**
 * @brief 行为同步任务a
 */
void app_sem_sync_a_task(void *p_arg)
{
    while (1)                                            
    {
        OSSchedLock();
        printf("sem sync A\r\n");
        OSSchedUnlock();
        OSSemPost(sem_sync_a);
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OSTimeDly(DELAY_TIMER);
    }
}

/**
 * @brief 行为同步任务b
 */
void app_sem_sync_b_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSSemPend(sem_sync_a, 0, &err);
        OSSchedLock();
        printf("sem sync B\r\n");
        OSSchedUnlock();
        OSSemPost(sem_sync_b);
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 行为同步任务c
 */
void app_sem_sync_c_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSSemPend(sem_sync_b, 0, &err);
        OSSchedLock();
        printf("sem sync C\r\n\r\n");
        OSSchedUnlock();
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 双向同步任务A
 */
void app_bisynchronous_a_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSMboxPost(sem_box, (void *)1);
        OSSchedLock();
        printf("bisynchronous A\r\n");
        OSSchedUnlock();
        OSMboxPend(ack_box, 0, &err);

        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OSTimeDly(DELAY_TIMER);
    }
}

/**
 * @brief 双向同步任务B
 */
void app_bisynchronous_b_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSMboxPend(sem_box, 0, &err);
        OSSchedLock();
        printf("bisynchronous B\r\n\r\n");
        OSSchedUnlock();
        OSMboxPost(ack_box, (void *)1);
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 事件同步任务A
 */
void app_flag_sync_a_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSSchedLock();
        printf("flag sync A\r\n");
        OSSchedUnlock();
        OSFlagPost(flagw,
                    0x01, 
                    OS_FLAG_SET,
                    &err);
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OSTimeDly(DELAY_TIMER);
    }
}

/**
 * @brief 事件同步任务B
 */
void app_flag_sync_b_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSSchedLock();
        printf("flag sync B\r\n");
        OSSchedUnlock();
        OSFlagPost(flagw,
                    0x02, 
                    OS_FLAG_SET,
                    &err);
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
        OSTimeDly(DELAY_TIMER);
    }
}

/**
 * @brief 事件同步任务C
 */
void app_flag_sync_c_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSFlagPend(flagw,
                    0x03, 
                    OS_FLAG_WAIT_SET_ALL
                    + OS_FLAG_CONSUME,
                    0,
                    &err);
        OSSchedLock();
        printf("flag sync C\r\n\r\n");
        OSSchedUnlock();
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    INT8U err;
    sem_isr = OSSemCreate(0);
    sem_sync_a = OSSemCreate(0);
    sem_sync_b = OSSemCreate(0);
    sem_sync_c = OSSemCreate(0);

    sem_box = OSMboxCreate((void *)0);
    ack_box = OSMboxCreate((void *)0);

    flagw = OSFlagCreate(0, &err);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{ 
    OSTaskCreate(
                (void (*)(void *)) app_sem_sync_a_task, 
                (void * ) 0,
                (OS_STK* )&app_sync_a_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO
                );
    
    OSTaskCreate(
                (void (*)(void *)) app_sem_sync_b_task, 
                (void * ) 0,
                (OS_STK* )&app_sync_b_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 1
                );

    OSTaskCreate(
                (void (*)(void *)) app_sem_sync_c_task, 
                (void * ) 0,
                (OS_STK* )&app_sync_c_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 2
                );
                
    OSTaskCreate(
                (void (*)(void *)) app_bisynchronous_a_task, 
                (void * ) 0,
                (OS_STK* )&app_bisynchronous_a_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 3
                );

    OSTaskCreate(
                (void (*)(void *)) app_bisynchronous_b_task, 
                (void * ) 0,
                (OS_STK* )&app_bisynchronous_b_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 4
                );
                
    OSTaskCreate(
                (void (*)(void *)) app_flag_sync_a_task, 
                (void * ) 0,
                (OS_STK* )&app_flag_sync_a_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 5
                );
                
    OSTaskCreate(
                (void (*)(void *)) app_flag_sync_b_task, 
                (void * ) 0,
                (OS_STK* )&app_flag_sync_b_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 6
                );
                
    OSTaskCreate(
                (void (*)(void *)) app_flag_sync_c_task, 
                (void * ) 0,
                (OS_STK* )&app_flag_sync_c_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 7
                );

    OSTaskCreate(
                (void (*)(void *)) app_isr_sync_task, 
                (void * ) 0,
                (OS_STK* )&app_isr_sync_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 8
                );
}

#if (OS_APP_HOOKS_EN > 0)

void App_TaskCreateHook(OS_TCB *ptcb)
{

}

void App_TaskDelHook(OS_TCB *ptcb)
{
    (void)ptcb;
}

#if OS_VERSION >= 251
void App_TaskIdleHook(void)
{
}
#endif

void App_TaskStatHook(void)
{
}


#if OS_TASK_SW_HOOK_EN > 0
void App_TaskSwHook(void)
{

}
#endif

#if OS_VERSION >= 204
void App_TCBInitHook(OS_TCB *ptcb)
{
    (void)ptcb;
}
#endif


#if OS_TIME_TICK_HOOK_EN > 0
void App_TimeTickHook(void)
{

}
#endif
#endif
