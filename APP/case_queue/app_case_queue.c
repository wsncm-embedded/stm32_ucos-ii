 /**
 * @brief       : 消息队列 测试用例
 * @file        : app_case_queue.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/10/12  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

#define QEUE_MAX_LEN 3
#define DATA_MAX_LEN sizeof(student_info_all[0])

OS_EVENT *sem_isr;

OS_STK app_task_start_stk[APP_TASK_START_STK_SIZE];
OS_STK app_producer_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_consumer_task_stk[App_TASK_COMMON_STK_SIZE];

typedef struct student_info_t_
{
    char name[20];
    char age[4];
} student_info_t;

student_info_t student_info_all[3] = 
{
	"Hanmeimei","23\0",
	"Lilei","24\0",
	"Lucy","25\0"
};

OS_EVENT *msg_qeue;
void *msg_qeue_tb[QEUE_MAX_LEN];

OS_MEM *partition_pt;
INT8U partition[QEUE_MAX_LEN][DATA_MAX_LEN];

static void app_task_create(void);
static void app_event_create(void);
static void app_task_start(void *p_arg);

int main(void)
{
    BSP_IntDisAll();                                            
    OSInit();                                                  
    OSTaskCreate(
                (void (*)(void *)) app_task_start, 
                (void * ) 0,
                (OS_STK* )&app_task_start_stk[APP_TASK_START_STK_SIZE - 1],
                (INT8U) APP_TASK_START_PRIO
                );
                
    OSStart();
    return (0);
}

/**
 * @brief 启动任务，单次执行
 */
void app_task_start(void *p_arg)
{
    (void)p_arg;
    OS_CPU_SysTickInit();  
    BSP_Init();
    app_event_create();
    app_task_create();
    while (1)                                            
    {
        printf("start task!\r\n");
        OSTimeDly(DELAY_TIMER);
        OSTaskDel(OS_PRIO_SELF);
    }
}

/**
 * @brief 生产者任务(消息队列)
 */
void app_qeue_producer_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        static INT8U run_count =  0;
        student_info_t *pt, *pt_temp, student_info_temp = student_info_all[run_count];
        
        pt = OSMemGet(partition_pt, &err);
        pt_temp = pt;
        memcpy(pt, &student_info_temp, DATA_MAX_LEN);
        
        OSSchedLock();
        printf("qeue producer post student info:\r\nname: %s\r\nage: %s\r\n",pt->name,pt->age);
        OSSchedUnlock();
        
        OSQPost(msg_qeue, (void *)pt_temp);
        RUN_COUNTER(run_count);
        OSTimeDly(DELAY_TIMER);
    }
}

/**
 * @brief 消费者任务(消息队列)
 */
void app_qeue_consumer_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        student_info_t *pt, *pt0;
        
        pt = OSQPend(msg_qeue, 0, &err);
        pt0 = pt;
        
        OSSchedLock();
        printf("qeue producer post student info:\r\nname: %s\r\nage: %s\r\n\r\n",pt->name,pt->age);
        OSSchedUnlock();
        
        OSMemPut(partition_pt, pt0);
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    INT8U err;
    
    msg_qeue = OSQCreate(&msg_qeue_tb[0], QEUE_MAX_LEN);
    partition_pt = OSMemCreate(partition, QEUE_MAX_LEN, DATA_MAX_LEN, &err);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{ 
    OSTaskCreate(
                (void (*)(void *)) app_qeue_producer_task, 
                (void * ) 0,
                (OS_STK* )&app_producer_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 1
                );
                
    OSTaskCreate(
                (void (*)(void *)) app_qeue_consumer_task, 
                (void * ) 0,
                (OS_STK* )&app_consumer_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO
                );
}

#if (OS_APP_HOOKS_EN > 0)

void App_TaskCreateHook(OS_TCB *ptcb)
{

}

void App_TaskDelHook(OS_TCB *ptcb)
{
    (void)ptcb;
}

#if OS_VERSION >= 251
void App_TaskIdleHook(void)
{
}
#endif

void App_TaskStatHook(void)
{
}


#if OS_TASK_SW_HOOK_EN > 0
void App_TaskSwHook(void)
{

}
#endif

#if OS_VERSION >= 204
void App_TCBInitHook(OS_TCB *ptcb)
{
    (void)ptcb;
}
#endif


#if OS_TIME_TICK_HOOK_EN > 0
void App_TimeTickHook(void)
{

}
#endif
#endif
