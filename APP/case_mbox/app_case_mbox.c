 /**
 * @brief       : 数据通信 测试用例
 *                消息邮箱：POST和PEND数据相同则测试通过
 * @file        : app_case_communicate.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/10/12  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

OS_STK app_task_start_stk[APP_TASK_START_STK_SIZE];
OS_STK app_mbox_producer_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_mbox_consumer_task_stk[App_TASK_COMMON_STK_SIZE];

static void app_task_create(void);
static void app_event_create(void);
static void app_task_start(void *p_arg);

OS_EVENT *mbox;
OS_EVENT *sem_isr;

int main(void)
{
    BSP_IntDisAll();                                            
    OSInit();                                                  
    OSTaskCreate(
                (void (*)(void *)) app_task_start, 
                (void * ) 0,
                (OS_STK* )&app_task_start_stk[APP_TASK_START_STK_SIZE - 1],
                (INT8U) APP_TASK_START_PRIO
                );
                
    OSStart();
    return (0);
}

/**
 * @brief 启动任务，单次执行
 */
void app_task_start(void *p_arg)
{
    (void)p_arg;
    OS_CPU_SysTickInit();  
    BSP_Init();
    app_event_create();
    app_task_create();
    while (1)                                            
    {
        printf("start task!\r\n");
        OSTimeDly(DELAY_TIMER);
        OSTaskDel(OS_PRIO_SELF);
    }
}

/**
 * @brief 生产者任务(消息邮箱)
 */
void app_mbox_producer_task(void *p_arg)
{
    while (1)                                            
    {
        static uint32_t samp =  0;
        static uint32_t run_count =  0;
        samp = rand();
        
        OSSchedLock();
        printf("mbox producer post samp: %d\r\n", samp);
        OSSchedUnlock();
        OSMboxPost(mbox, (void *)&samp);
        RUN_COUNTER(run_count);
        OSTimeDly(DELAY_TIMER);
    }
}

/**
 * @brief 消费者任务(消息邮箱)
 */
void app_mbox_consumer_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        static uint32_t run_count =  0;
        uint32_t temp = *(uint32_t *)OSMboxPend(mbox, 0, &err);
        
        OSSchedLock();
        printf("mbox consumer pend samp: %d\r\n\r\n", temp);
        OSSchedUnlock();
        
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    mbox = OSMboxCreate((void *)0);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{ 
    OSTaskCreate(
                (void (*)(void *)) app_mbox_consumer_task, 
                (void * ) 0,
                (OS_STK* )&app_mbox_consumer_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO
                );

    OSTaskCreate(
                (void (*)(void *)) app_mbox_producer_task, 
                (void * ) 0,
                (OS_STK* )&app_mbox_producer_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 1
                );
}

#if (OS_APP_HOOKS_EN > 0)

void App_TaskCreateHook(OS_TCB *ptcb)
{

}

void App_TaskDelHook(OS_TCB *ptcb)
{
    (void)ptcb;
}

#if OS_VERSION >= 251
void App_TaskIdleHook(void)
{
}
#endif

void App_TaskStatHook(void)
{
}


#if OS_TASK_SW_HOOK_EN > 0
void App_TaskSwHook(void)
{

}
#endif

#if OS_VERSION >= 204
void App_TCBInitHook(OS_TCB *ptcb)
{
    (void)ptcb;
}
#endif


#if OS_TIME_TICK_HOOK_EN > 0
void App_TimeTickHook(void)
{

}
#endif
#endif
