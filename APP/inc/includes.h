#ifndef  __INCLUDES_H
#define  __INCLUDES_H

#include  <stdio.h>
#include  <string.h>
#include  <ctype.h>
#include  <stdlib.h>
#include  <stdarg.h>
#include  <ucos_ii.h>
#include  <cpu.h>
#include  <lib_def.h>
#include  <lib_mem.h>
#include  <lib_str.h>
#include  <stm32f10x.h>
#include  <stm32f10x_it.h>
#include  <app_cfg.h>
#include  <bsp.h>

#define RUN_TIMERS 3
#define DELAY_TIMER 1000

#define   RUN_COUNTER(X)        if(X++ >= RUN_TIMERS - 1)\
								{\
									OSTaskDel(OS_PRIO_SELF);\
								}

#endif
