 /**
 * @brief       : 系统时间管理
 *                系统节拍: 测试任务周期执行，打印时间间隔。
 *                真实时间: 测试任务周期执行，打印时间间隔。
 *                取消延时: 中断中发送'1'来取消系统节拍延时任务，打印当前时间。
 * @file        : app_case_delay.c
 * @author      : cuihongpeng
 * @version     : v0.0.1
 * @date        : 2015/10/12
 *
 * Change Logs  : 
 *
 * Date        Version      Author      Notes
 * 2015/10/12  v0.0.1    cuihongpeng    first version
 */
#include <includes.h>

OS_STK app_task_start_stk[APP_TASK_START_STK_SIZE];
OS_STK app_delay_by_sys_beat_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_delay_by_real_time_task_stk[App_TASK_COMMON_STK_SIZE];
OS_STK app_delay_resume_task_stk[App_TASK_COMMON_STK_SIZE];

static void app_task_create(void);
static void app_event_create(void);
static void app_task_start(void *p_arg);

OS_EVENT *sem_isr;                  /**< 中断到任务的信号量 */
OS_EVENT *sem_resume_delay;         /**< 取消延时信号量 */

int main(void)
{
    BSP_IntDisAll();                                            
    OSInit();                                                  
    OSTaskCreate(
                (void (*)(void *)) app_task_start, 
                (void * ) 0,
                (OS_STK* )&app_task_start_stk[APP_TASK_START_STK_SIZE - 1],
                (INT8U) APP_TASK_START_PRIO
                );
                
    OSStart();
    return (0);
}

/**
 * @brief 启动任务，单次执行
 */
void app_task_start(void *p_arg)
{
    (void)p_arg;
    OS_CPU_SysTickInit();  
    BSP_Init();
    app_event_create();
    app_task_create();
    while (1)                                            
    {
        printf("start task!\r\n");
        OSTimeDly(DELAY_TIMER);
        OSTaskDel(OS_PRIO_SELF);
    }
}

/**
 * @brief 调用系统延时函数 OSTimeDly
 */
void app_delay_by_sys_beat_task(void *p_arg)
{
    while (1)                                            
    {
        INT32U previous_time = OSTimeGet();
        OSTimeDly(DELAY_TIMER);
        
        INT32U last_time = OSTimeGet();
        INT32U offset_time = last_time - previous_time;
        
        OSSchedLock();
        printf("OSTimeDly task: %d\r\n", offset_time);
        OSSchedUnlock();
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 调用系统延时函数 OSTimeDlyHMSM()
 */
void app_delay_by_real_time_task(void *p_arg)
{
    while (1)                                            
    {
        INT32U previous_time = OSTimeGet();
        OSTimeDlyHMSM(0,0,1,0);
        
        INT32U last_time = OSTimeGet();
        INT32U offset_time = last_time - previous_time;
        
        OSSchedLock();
        printf("OSTimeDlyHMSM task: %d\r\n\r\n", offset_time);
        OSSchedUnlock();
        
        static uint32_t run_count =  0;
        RUN_COUNTER(run_count);
    }
}

/**
 * @brief 取消延时任务
 */
void app_delay_resume_task(void *p_arg)
{
    while (1)                                            
    {
        INT8U err;
        OSSemPend(sem_isr, 0, &err);
        OSTimeDlyResume(APP_TASK_COMMON_PRIO);
    }
}

/**
 * @brief 创建信号量
 */
static void app_event_create(void)
{
    sem_isr = OSSemCreate(0);
    sem_resume_delay = OSSemCreate(0);
}

/**
 * @brief 创建任务
 */
static void app_task_create(void)
{
    OSTaskCreate(
                (void (*)(void *)) app_delay_by_sys_beat_task, 
                (void * ) 0,
                (OS_STK* )&app_delay_by_sys_beat_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO
                );
                
    OSTaskCreate(
                (void (*)(void *)) app_delay_by_real_time_task, 
                (void * ) 0,
                (OS_STK* )&app_delay_by_real_time_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 1
                );
                
    OSTaskCreate(
                (void (*)(void *)) app_delay_resume_task, 
                (void * ) 0,
                (OS_STK* )&app_delay_resume_task_stk[App_TASK_COMMON_STK_SIZE - 1],
                (INT8U) APP_TASK_COMMON_PRIO + 2
                );
}



#if (OS_APP_HOOKS_EN > 0)

void App_TaskCreateHook(OS_TCB *ptcb)
{

}

void App_TaskDelHook(OS_TCB *ptcb)
{
    (void)ptcb;
}

#if OS_VERSION >= 251
void App_TaskIdleHook(void)
{
}
#endif

void App_TaskStatHook(void)
{
}


#if OS_TASK_SW_HOOK_EN > 0
void App_TaskSwHook(void)
{

}
#endif

#if OS_VERSION >= 204
void App_TCBInitHook(OS_TCB *ptcb)
{
    (void)ptcb;
}
#endif


#if OS_TIME_TICK_HOOK_EN > 0
void App_TimeTickHook(void)
{

}
#endif
#endif
